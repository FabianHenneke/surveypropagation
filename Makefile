OBJECTS = src/main.o src/SATFormula.o src/SurveyPropagation.o src/UnitClausePropagation.o src/WalkSAT.o                                                        

CXXFLAGS = -Wall -O3 -funroll-loops -std=c++11                                                       

all: survey verify

survey: $(OBJECTS)
	$(CXX) $(CXXFLAGS) -o bin/survey $(OBJECTS)

verify: 
	$(CXX) $(CXXFLAGS) -o bin/verify src/verify.cc