/*
 * UnitClausePropagation.h
 *
 *  Created on: 24.12.2014
 *      Author: fabian
 */

#ifndef SRC_UNITCLAUSEPROPAGATION_H_
#define SRC_UNITCLAUSEPROPAGATION_H_

#include "SATFormula.h"

class UnitClausePropagation
{
public:
	UnitClausePropagation() = delete;
	explicit UnitClausePropagation(SATFormula & formula);

	bool reduce();

private:

	SATFormula& formula_;
};

#endif /* SRC_UNITCLAUSEPROPAGATION_H_ */
