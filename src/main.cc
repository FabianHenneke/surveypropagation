/* -*- mode: C++; indent-tabs-mode: nil; -*-
 *
 * This file is a part of LEMON, a generic C++ optimization library.
 *
 * Copyright (C) 2003-2009
 * Egervary Jeno Kombinatorikus Optimalizalasi Kutatocsoport
 * (Egervary Research Group on Combinatorial Optimization, EGRES).
 *
 * Permission to use, modify and distribute this software is granted
 * provided that this copyright notice appears in all copies. For
 * precise terms see the accompanying LICENSE file.
 *
 * This software is provided "AS IS" with no warranty of any kind,
 * express or implied, and with no claim as to its suitability for any
 * purpose.
 *
 */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>

#include <tclap/CmdLine.h>

#include "SATFormula.h"
#include "WalkSAT.h"
#include "UnitClausePropagation.h"
#include "SurveyPropagation.h"

using namespace std;

///The main entry point
int main(int argc, char** argv)
{
	try
	{
		TCLAP::CmdLine cmd("", ' ', "1.0");

		TCLAP::ValueArg<std::size_t> kArg("k", "k", "# of literals in a clause",
				false, 3, "uint", cmd);
		TCLAP::ValueArg<double> alphaArg("a", "alpha",
				"Ratio of #clauses to #variables", false, 4.0, "double", cmd);

		TCLAP::ValueArg<std::size_t> nArg("n", "n", "# of variables", false,
				4000, "uint");
		TCLAP::UnlabeledValueArg<std::string> inputArg("input",
				"Input file (CNF formula, DIMACS format)", false, "",
				"filename");
		cmd.xorAdd(nArg, inputArg);

		TCLAP::ValueArg<std::string> outputArg("o", "output",
				"Output file (assignment, DIMACS format)", false, "",
				"filename", cmd);

		TCLAP::ValueArg<std::string> simplifiedArg("s", "simplified",
				"Output file (simplified formula, DIMACS format)", false, "",
				"filename", cmd);

		TCLAP::ValueArg<std::string> originalArg("f", "formula",
				"Output file (original formula, DIMACS format)", false, "",
				"filename", cmd);

		TCLAP::SwitchArg walksatSwitch("w", "walksat", "Only use WalkSAT", cmd);

		TCLAP::SwitchArg verifySwitch("v", "verify",
				"Verify solution (-o and -f required)", cmd, false);

		TCLAP::ValueArg<double> blockDecimationArg("", "dblock",
				"Fraction of variables to fix per iteration", false, 0.01,
				"double", cmd);
		TCLAP::ValueArg<double> rhoArg("", "rho", "rho (0.0: BP, 1.0: SP)",
				false, 1.0, "double", cmd);
		TCLAP::ValueArg<double> epsArg("", "eps",
				"epsilon (convergence threshold)", false, 0.01, "double", cmd);
		TCLAP::ValueArg<std::size_t> spiterArg("", "spiter",
				"max # of iterations for an SP step", false, 100, "uint", cmd);
		TCLAP::SwitchArg complexitySwitch("c", "complexity",
				"Compute complexity", cmd);

		cmd.parse(argc, argv);

		SATFormula S;

		if (nArg.isSet())
		{
			S = SATFormula(kArg.getValue(), nArg.getValue(),
					alphaArg.getValue());
		}
		else
		{
			S = SATFormula(inputArg.getValue());
		}

		if (originalArg.isSet())
		{
			std::cout << "# Writing generated formula to \""
					<< originalArg.getValue() << "\"..." << std::endl;
			std::ofstream orig(originalArg.getValue());
			S.writeFormula(orig);
		}

		bool result = true;
		if (!walksatSwitch.getValue())
		{
			SurveyPropagation SP(S);

			if (blockDecimationArg.isSet())
			{
				SP.D_BLOCK = true;
				SP.D_FRACTION = blockDecimationArg.getValue();
			}

			SP.RHO = rhoArg.getValue();
			SP.EPS = epsArg.getValue();
			SP.MAX_ITERS = spiterArg.getValue();
			SP.COMPLEXITY = complexitySwitch.getValue();

			result = SP.simplify();

			if (simplifiedArg.isSet())
			{
				std::cout << "# Writing simplified formula to \""
						<< simplifiedArg.getValue() << "\"..." << std::endl;
				std::ofstream simp(simplifiedArg.getValue());
				S.writeFormula(simp);
			}
		}

		if (result)
		{
			WalkSAT walk(S);
			result = walk.solve();
		}
		else
		{
			return EXIT_FAILURE;
		}

		if (outputArg.isSet())
		{
			std::cout << "# Writing assignment to \"" << outputArg.getValue()
					<< "\"..." << std::endl;
			std::ofstream out(outputArg.getValue());
			S.writeAssignment(out);
		}

		if (verifySwitch.isSet() && originalArg.isSet() && outputArg.isSet())
		{
			std::cout << "# Invoking: ./verify " << originalArg.getValue() << " "
					<< outputArg.getValue() << std::endl;
			system(
					("./bin/verify " + originalArg.getValue() + " "
							+ outputArg.getValue()).c_str());
		}

		if (result)
			return EXIT_SUCCESS;
		else
			return EXIT_FAILURE;

	} catch (TCLAP::ArgException &e)  // catch any exceptions
	{
		std::cerr << "error: " << e.error() << " for arg " << e.argId()
				<< std::endl;
	}

}
