/*
 * WalkSAT.h
 *
 *  Created on: 23.12.2014
 *      Author: fabian
 */

#ifndef SRC_WALKSAT_H_
#define SRC_WALKSAT_H_

#include <random>
#include <chrono>
#include <cassert>
#include <tuple>
#include <iomanip>

#include "SATFormula.h"

class WalkSAT
{
public:
	WalkSAT() = delete;
	explicit WalkSAT(SATFormula& formula);

	bool solve();

	std::size_t MAX_FLIPS = 10000;
	std::size_t MAX_TRIES = 12;

	float p = 0.5;
	float wp = 0.01;

private:

	void generateRandomAssignment();
	void evaluate();
	std::tuple<VarID, VarID, VarID, bool> localSearch(ClauseID clause_id);
	void flipAndUpdate(VarID var_id);
	VarID randomVariableFromClause(ClauseID clause_id);
	ClauseID chooseUnsatisfiedClause();

	SATFormula& formula_;

	std::vector<std::size_t> num_true_lit_;
	std::vector<std::ptrdiff_t> flip_rating_;
	std::vector<std::size_t> last_flip_;
	std::vector<ClauseID> unsatisfied_clauses_;
	std::vector<std::size_t> unsatisfied_clauses_id_;

	std::size_t tries_ = 0;
	std::size_t flips_ = 0;
	std::size_t num_unsatisfied_ = 0;

	std::default_random_engine gen_;
};

#endif /* SRC_WALKSAT_H_ */
