#include <cstdlib>
#include <iostream>
#include <fstream>
#include <limits>
#include <vector>

enum class TRUTH_VALUE
{
	FALSE, TRUE, UNSPECIFIED
};

std::vector<TRUTH_VALUE> solution_;

void read_solution(char* solution_file)
{
	std::ifstream sol(solution_file);

	if (!sol.good())
	{
		std::cout << "Error: cannot read solution file " << solution_file
				<< std::endl;
		exit(EXIT_FAILURE);
	}

	std::size_t max_index = 0;

	while (!sol.eof())
	{
		char command;

		sol >> command;

		if (command != 'v')
		{
			sol.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			continue;
		}

		while (sol.peek() != '\n')
		{
			long literal;

			sol >> literal;
			max_index = std::max((std::size_t) std::abs(literal), max_index);
		}
	}

	solution_.resize(max_index, TRUTH_VALUE::UNSPECIFIED);
	sol.clear();
	sol.seekg(0, std::ios::beg);

	while (!sol.eof())
	{
		char command;

		sol >> command;

		if (command != 'v')
		{
			sol.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			continue;
		}

		while (sol.peek() != '\n')
		{
			long literal;

			sol >> literal;

			if (literal > 0)
			{
				solution_[literal - 1] = TRUTH_VALUE::TRUE;
			}
			else
			{
				solution_[-literal - 1] = TRUTH_VALUE::FALSE;
			}
		}
	}
}

bool verify_solution(char* instance_file)
{
	std::ifstream ins(instance_file);

	if (!ins.good())
	{
		std::cout << "Error: cannot read instance file " << instance_file
				<< std::endl;
		exit(EXIT_FAILURE);
	}

	bool header = true;

	while (!ins.eof())
	{
		if (header)
		{
			char command;

			ins >> command;

			if (command == 'p')
				header = false;

			ins.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		}
		else
		{
			bool satisfied = false;
			long literal;

			ins >> literal;

			while (literal != 0)
			{
				bool sign = (literal > 0);
				std::size_t index = std::abs(literal);

				if (index > solution_.size())
					return false;

				TRUTH_VALUE truth_value = solution_[index];

				if ((truth_value == TRUTH_VALUE::TRUE && sign)
						|| (truth_value == TRUTH_VALUE::FALSE && !sign))
				{
					satisfied = true;
					break;
				}

				ins >> literal;
			}

			ins.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

			if (!satisfied)
				return false;
		}
	}

	return true;
}

int main(int argc, char** argv)
{
	if (argc != 3)
	{
		std::cout << "Usage: " << argv[0] << " instance solution\n";
		return EXIT_FAILURE;
	}

	read_solution(argv[1]);

	if (verify_solution(argv[2]))
	{
		std::cout << "SATISFIED\n";
		return EXIT_SUCCESS;
	}
	else
	{
		std::cout << "UNSATISFIED\n";
		return EXIT_FAILURE;
	}
}

