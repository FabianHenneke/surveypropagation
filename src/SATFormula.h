#ifndef SRC_SATFORMULA_H_
#define SRC_SATFORMULA_H_

#include <vector>
#include <iostream>
#include <fstream>
#include <limits>
#include <stdexcept>
#include <algorithm>
#include <random>
#include <chrono>

typedef std::size_t ClauseID;
typedef std::size_t VarID;
typedef std::size_t EdgeID;

class WalkSAT;
class UnitClausePropagation;

struct VarClauseEdge
{
	VarClauseEdge(VarID var_id, ClauseID clause_id, EdgeID edge_id, bool sign) :
			var_id(var_id), clause_id(clause_id), edge_id(edge_id), sign(sign)
	{

	}

	VarID var_id;
	ClauseID clause_id;
	EdgeID edge_id;
	bool sign;
};

struct ClauseVarEdge
{
	ClauseVarEdge(ClauseID clause_id, VarID var_id, EdgeID edge_id, bool sign) :
			clause_id(clause_id), var_id(var_id), edge_id(edge_id), sign(sign)
	{

	}

	ClauseID clause_id;
	VarID var_id;
	EdgeID edge_id;
	bool sign;
};

class SATFormula
{

	friend WalkSAT;
	friend UnitClausePropagation;

public:

	SATFormula();
//explicit SATFormula_Template(size_t n);
//	SATFormula(std::size_t k, std::size_t n, std::size_t m);
	SATFormula(std::size_t k, std::size_t n, double alpha);

	explicit SATFormula(const std::string &filename);

	void update();
	void print() const;
	void printAssignment() const;

	void writeFormula(std::ostream& out) const;
	void writeAssignment(std::ostream& out) const;

	void setVar(const VarID var_id, const bool value);
	bool getVar(const VarID var_id) const;

	bool fixVar(const VarID var_id);
	void flipVar(const VarID var_id);

	bool varFixed(const VarID var_id) const;
	bool clauseFixed(const ClauseID clause_id) const;

	VarClauseEdge& dualEdge(const ClauseVarEdge& cv);
	ClauseVarEdge& dualEdge(const VarClauseEdge& vc);

//	std::vector<bool>::reference operator[](const VarID var_id);

//	const std::size_t getVarInClause(const std::size_t clause_id,
//			const std::size_t var_pos);

	bool evaluate() const;
//	std::size_t numTrueLitInClause(std::size_t clause_id);

	std::size_t numUnfixedVariables() const;
	std::size_t numUnfixedClauses() const;

	std::size_t numVariables() const;
	std::size_t numVariables(ClauseID clause_id) const;
	std::size_t numClauses() const;
	std::size_t numClauses(VarID var_id) const;
	std::size_t numEdges() const;

//	template<class Visitor>
//	void visitVarClauseEdges(const Visitor& visitor);

	template<class Visitor>
	void visitClauseVarEdges(const Visitor& visitor);

	template<class Visitor>
	void visitClauseVarEdges(const ClauseID clause_id, const Visitor& visitor);

	template<class Visitor>
	void visitVarClauseEdges(const VarID var_id, const Visitor& visitor);

private:

	void fixClause(ClauseID clause_id, ClauseID var_id);
	std::vector<VarClauseEdge>::iterator dualEdgeIt(const ClauseVarEdge& cv);
	std::vector<ClauseVarEdge>::iterator dualEdgeIt(const VarClauseEdge& cv);

	VarID n_;
	ClauseID m_;

	std::size_t edges_;

	std::vector<std::vector<VarClauseEdge>> var_to_clause_;
	std::vector<std::vector<ClauseVarEdge>> clause_to_var_;

	std::vector<bool> assignment_;
	std::vector<bool> var_fixed_;
	std::vector<bool> clause_fixed_;

	bool contradiction_;
	std::size_t unfixed_vars_;
	std::size_t unfixed_clauses_;


};

/*template<class Visitor>
void SATFormula::visitVarClauseEdges(const Visitor& visitor)
{
	for (auto& var : var_to_clause_)
	{
		for (auto& vc : var)
		{
			visitor(vc);
		}
	}
}*/

template<class Visitor>
void SATFormula::visitClauseVarEdges(const Visitor& visitor)
{
	for (auto& clause : clause_to_var_)
	{
		for (auto& cv : clause)
		{
			visitor(cv);
		}
	}
}

template<class Visitor>
void SATFormula::visitClauseVarEdges(const ClauseID clause_id,
		const Visitor& visitor)
{

	for (auto& cv : clause_to_var_[clause_id])
	{
		visitor(cv);
	}

}

template<class Visitor>
void SATFormula::visitVarClauseEdges(const VarID var_id, const Visitor& visitor)
{

	for (auto& vc : var_to_clause_[var_id])
	{
		visitor(vc);
	}

}

#endif
