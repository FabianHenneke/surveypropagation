#include "SATFormula.h"

void SATFormula::setVar(const VarID var_id, const bool value)
{
	assignment_[var_id] = value;
}

bool SATFormula::getVar(const VarID var_id) const
{
	return assignment_[var_id];
}

void SATFormula::flipVar(const VarID var_id)
{
	assignment_[var_id] = !assignment_[var_id];
}

std::size_t SATFormula::numUnfixedVariables() const
{
	return unfixed_vars_;
}

std::size_t SATFormula::numUnfixedClauses() const
{
	return unfixed_clauses_;
}

std::size_t SATFormula::numVariables() const
{
	return n_;
}

std::size_t SATFormula::numVariables(ClauseID clause_id) const
{
	return clause_to_var_[clause_id].size();
}

std::size_t SATFormula::numClauses() const
{
	return m_;
}

std::size_t SATFormula::numClauses(VarID var_id) const
{
	return var_to_clause_[var_id].size();
}

std::size_t SATFormula::numEdges() const
{
	return edges_;
}

SATFormula::SATFormula() :
		n_(0), m_(0), edges_(0), contradiction_(false), unfixed_vars_(0), unfixed_clauses_(
				0)
{

}

SATFormula::SATFormula(std::size_t k, std::size_t n, double alpha) :
		n_(n), m_(static_cast<std::size_t>(std::round(alpha * n))), contradiction_(
				false), unfixed_vars_(n), unfixed_clauses_(m_)
{
	if (k > n)
		throw std::logic_error(
				"# Error: number of variables is smaller than clause length");

	std::cout << "# Generating " << k << "-SAT formula, n=" << n_ << ", m="
			<< m_ << ", alpha=" << alpha << "..." << std::endl;

	var_to_clause_.resize(n_);
	assignment_.resize(n_);
	var_fixed_.resize(n_, false);

	clause_to_var_.resize(m_);
	clause_fixed_.resize(m_, false);

	std::default_random_engine gen_(
			std::chrono::system_clock::now().time_since_epoch().count());
	auto sign_gen = std::bind(std::bernoulli_distribution(), gen_);
	auto var_gen = std::bind(std::uniform_int_distribution<VarID>(0, n_ - 1),
			gen_);

	edges_ = 0;

	for (ClauseID clause = 0; clause < m_; ++clause)
	{
		for (std::size_t literal = 0; literal < k; ++literal)
		{
			VarID var = var_gen();
			while (std::find_if(clause_to_var_[clause].cbegin(),
					clause_to_var_[clause].cend(),
					[var](const ClauseVarEdge& cv)
					{	return cv.var_id == var;})
					!= clause_to_var_[clause].cend())
				var = var_gen();
			bool sign = sign_gen();

			var_to_clause_[var].emplace_back(var, clause, edges_, sign);
			clause_to_var_[clause].emplace_back(clause, var, edges_, sign);
			++edges_;
		}
	}
}

SATFormula::SATFormula(const std::string &filename) :
		n_(0), m_(0), contradiction_(false), unfixed_vars_(0)
{
	std::cout << "# Reading formula from \"" << filename << "\"..." << std::endl;

	std::ifstream in(filename, std::ifstream::in);

	if (!in.good())
	{
		throw std::logic_error("# Error: Could not open " + filename);
	}

	bool header = true;

	ClauseID clause_id = 1;
	edges_ = 0;

	while (!in.eof())
	{
		if (header)
		{
			char prefix;
			in >> prefix;

			std::string format;

			switch (prefix)
			{
			case 'c':
				//skip the rest of the line, a comment
				in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				break;

			case 'p':
				//p line contains the number of variables and clauses

				in >> format;

				if (format != "cnf")
					throw std::logic_error(
							"# Error: Format " + format + " is not supported");

				in >> n_;
				if (n_ <= 0)
					throw std::logic_error(
							"# Error: Number of variables has to be positive");

				in >> m_;
				if (m_ < 0)
					throw std::logic_error(
							"# Error: Number of clauses has to be non-negative");

				header = false;

				std::cout << "# Reading SAT formula, n=" << n_ << ", m=" << m_
						<< ", alpha=" << ((double) m_) / n_ << std::endl;

				var_to_clause_.resize(n_);
				assignment_.resize(n_);
				var_fixed_.resize(n_, false);

				clause_to_var_.resize(m_);
				clause_fixed_.resize(m_, false);

				break;

			default:
				throw std::logic_error(
						"# Error: Invalid command (only 'p' and 'c' are supported)");
			}
		}
		else
		{
			std::ptrdiff_t signed_id;

			in >> signed_id;

			bool sign;
			VarID var_id;

			if (signed_id == 0)
			{
				++clause_id;
				continue;
			}

			if (signed_id < 0)
			{
				var_id = -signed_id;
				sign = false;
			}
			else
			{
				var_id = signed_id;
				sign = true;
			}

			if (var_id > n_)
				throw std::logic_error(
						"# Error: Variable index " + std::to_string(var_id)
								+ " is out of bounds");

			clause_to_var_[clause_id - 1].emplace_back(clause_id - 1,
					var_id - 1, edges_, sign);
			var_to_clause_[var_id - 1].emplace_back(var_id - 1, clause_id - 1,
					edges_, sign);

			++edges_;

		}
	}
//-2 since eof is read as 0
	if (clause_id - 2 != m_)
		throw std::logic_error(
				"# Error: " + std::to_string(m_) + " clauses expected, got "
						+ std::to_string(clause_id - 2));

	unfixed_vars_ = n_;
	unfixed_clauses_ = m_;
}

bool SATFormula::evaluate() const
{
	if (contradiction_)
		return false;

	bool value;
	bool clause_value;

	value = true;
	for (ClauseID clause_id = 0; clause_id < m_; ++clause_id)
	{
		clause_value = clause_fixed_[clause_id];
		for (auto const &var : clause_to_var_[clause_id])
		{
			clause_value |=
					var.sign ?
							assignment_[var.var_id] : !assignment_[var.var_id];
		}

		value &= clause_value;
	}

	return value;
}

bool SATFormula::fixVar(const VarID var_id)
{
	if (!var_fixed_[var_id])
	{
		--unfixed_vars_;
		var_fixed_[var_id] = true;

		//Substitute the value for the variable in all clauses that reference it
		for (auto const &vc : var_to_clause_[var_id])
		{

			//Check whether the variable satisfies the clause
			if (vc.sign == assignment_[var_id])
			{
				//Clause is always satisfied, delete all its references to variables
				fixClause(vc.clause_id, var_id);
			}
			else
			{
				//Check whether var_id is the only variable left in the clause
				if (clause_to_var_[vc.clause_id].size() <= 1)
				{
					//As it is not satisfying, a contradiction has been reached
					contradiction_ = true;
					return false;
				}
				else
				{
					//remove the contribution to clause
					clause_to_var_[vc.clause_id].erase(dualEdgeIt(vc));
				}
			}
		}

//Delete all references from the variable to clauses
		edges_ -= var_to_clause_[var_id].size();
		var_to_clause_[var_id].clear();
	}
	return true;
}

void SATFormula::fixClause(ClauseID clause_id, VarID var_id)
{

	if (!clause_fixed_[clause_id])
	{
		clause_fixed_[clause_id] = true;
		--unfixed_clauses_;

//for all variables referenced by the clause
		for (auto const &cv : clause_to_var_[clause_id])
		{
//remove the clause from the list of clauses the variable appears in
			if (cv.var_id != var_id)
			{
				var_to_clause_[cv.var_id].erase(dualEdgeIt(cv));
				--edges_;
			}
		}

		clause_to_var_[clause_id].clear();
	}
}

bool SATFormula::varFixed(const VarID var_id) const
{
	return var_fixed_[var_id];
}
bool SATFormula::clauseFixed(const ClauseID clause_id) const
{
	return clause_fixed_[clause_id];
}

std::vector<VarClauseEdge>::iterator SATFormula::dualEdgeIt(
		const ClauseVarEdge& cv)
{
	return std::find_if(begin(var_to_clause_[cv.var_id]),
			end(var_to_clause_[cv.var_id]), [&cv](VarClauseEdge vc)
			{
				return vc.clause_id == cv.clause_id;
			});
}
std::vector<ClauseVarEdge>::iterator SATFormula::dualEdgeIt(
		const VarClauseEdge& vc)
{
	return std::find_if(begin(clause_to_var_[vc.clause_id]),
			end(clause_to_var_[vc.clause_id]), [&vc](ClauseVarEdge cv)
			{
				return cv.var_id == vc.var_id;
			});
}

VarClauseEdge& SATFormula::dualEdge(const ClauseVarEdge& cv)
{
	return *dualEdgeIt(cv);
}
ClauseVarEdge& SATFormula::dualEdge(const VarClauseEdge& vc)
{
	return *dualEdgeIt(vc);
}

void SATFormula::print() const
{

	std::cout << "CNF formula with " << std::to_string(n_) << " variables and "
			<< std::to_string(m_) << " clauses:\n";

	for (ClauseID clause_id = 0; clause_id < m_; ++clause_id)
	{
		std::cout << "Clause " << std::to_string(clause_id + 1) << ": ";

		if (clause_fixed_[clause_id])
			std::cout << "true";
		else
			for (auto const &var : clause_to_var_[clause_id])
				std::cout << (var.sign ? "" : "-")
						<< std::to_string(var.var_id + 1) << " ";
		std::cout << '\n';
	}
}

void SATFormula::printAssignment() const
{

	std::cout << "Assignment to " << std::to_string(n_) << " variables:\n";

	for (VarID var_id = 0; var_id < m_; ++var_id)
	{
		std::cout << "Variable " << std::to_string(var_id + 1) << ": ";

		std::cout << (assignment_[var_id] ? "true" : "false")
				<< (var_fixed_[var_id] ? " (fixed)\n" : "\n");
	}
}

void SATFormula::writeFormula(std::ostream& out) const
{
	if (!out.good())
		return;

	out << "p cnf " << n_ << " " << unfixed_clauses_ << std::endl;

	for (ClauseID clause = 0; clause < m_; ++clause)
	{
		if (clauseFixed(clause))
			continue;

		for (auto const& cv : clause_to_var_[clause])
		{
			out << (cv.sign ? (long) cv.var_id + 1 : (long) -(cv.var_id + 1))
					<< " ";
		}

		out << "0\n";
	}

	return;
}

void SATFormula::writeAssignment(std::ostream& out) const
{
	if (!out.good())
		return;

	if (!evaluate())
	{
		out << "s UNKNOWN" << std::endl;
		return;
	}

	out << "s SATISFIABLE" << std::endl;

	for (VarID var_id = 0; var_id < n_; ++var_id)
	{
		out << "v " << (assignment_[var_id] ? "-" : "") << (var_id + 1)
				<< std::endl;
	}

	return;
}

