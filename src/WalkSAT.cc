#include "WalkSAT.h"

WalkSAT::WalkSAT(SATFormula & formula) :
		formula_(formula), gen_(
				std::chrono::system_clock::now().time_since_epoch().count())
{
	flip_rating_.resize(formula_.numVariables());
	last_flip_.resize(formula_.numVariables());

	num_true_lit_.resize(formula_.numClauses());
	unsatisfied_clauses_id_.resize(formula_.numClauses());
}

void WalkSAT::generateRandomAssignment()
{
	for (VarID var_id = 0; var_id < formula_.numVariables(); ++var_id)
	{
		if (!formula_.var_fixed_[var_id])
		{
			VarID pos_literals = 0;
			for (auto const &vc : formula_.var_to_clause_[var_id])
			{
				if (vc.sign)
					++pos_literals;
			}

			auto coin = std::bernoulli_distribution(
					((double) pos_literals)
							/ formula_.var_to_clause_[var_id].size());

			formula_.setVar(var_id, coin(gen_));

		}
	}
}

void WalkSAT::evaluate()
{
	num_unsatisfied_ = 0;
	unsatisfied_clauses_.clear();

	std::fill(std::begin(flip_rating_), std::end(flip_rating_), 0);
	std::fill(std::begin(last_flip_), std::end(last_flip_), 0);

	for (ClauseID clause_id = 0; clause_id < formula_.numClauses(); ++clause_id)
	{
		if (formula_.clause_fixed_[clause_id])
			continue;

		VarID num_true_lit = 0;
		VarID true_var = 0;

		for (auto const &cv : formula_.clause_to_var_[clause_id])
		{
			//Does the literal satisfy the clause?
			if (cv.sign == formula_.assignment_[cv.var_id])
			{
				//If yes, increase number of true literals and memorize the true literal
				++num_true_lit;
				true_var = cv.var_id;
			}
		}

		num_true_lit_[clause_id] = num_true_lit;

		//Is clause unsatisfied?
		if (num_true_lit_[clause_id] == 0)
		{
			unsatisfied_clauses_id_[clause_id] = unsatisfied_clauses_.size();
			unsatisfied_clauses_.push_back(clause_id);

			++num_unsatisfied_;

			for (auto const &var : formula_.clause_to_var_[clause_id])
			{
				//If no, flipping the variable becomes more benefical
				++flip_rating_[var.var_id];
			}
		}
		//If the clause has only one true literal, flipping the corresponding variable becomes less benefical
		else if (num_true_lit_[clause_id] == 1)
		{
			--flip_rating_[true_var];
		}
	}
}

VarID WalkSAT::randomVariableFromClause(std::size_t clause_id)
{
	std::size_t var_pos = std::uniform_int_distribution<>(0,
			formula_.numVariables(clause_id) - 1)(gen_);

	return formula_.clause_to_var_[clause_id][var_pos].var_id;
}

ClauseID WalkSAT::chooseUnsatisfiedClause()
{
	assert(num_unsatisfied_ > 0);
	assert(num_unsatisfied_ == unsatisfied_clauses_.size());

	std::size_t unsatisfied_clause_pos = std::uniform_int_distribution<
			std::size_t>(0, num_unsatisfied_ - 1)(gen_);

	return unsatisfied_clauses_[unsatisfied_clause_pos];
}

std::tuple<VarID, VarID, VarID, bool> WalkSAT::localSearch(ClauseID clause_id)
{
	VarID best = 0;
	bool best_found = false;
	VarID second_best = 0;
	bool second_best_found = false;
	VarID last_flipped = 0;
	bool last_flipped_found = false;

	for (auto const &cv : formula_.clause_to_var_[clause_id])
	{
		VarID var_id = cv.var_id;

		if (formula_.var_fixed_[var_id])
			continue;

		if (!last_flipped_found
				|| last_flip_[var_id] > last_flip_[last_flipped])
		{
			last_flipped_found = true;
			last_flipped = var_id;
		}

		if (!best_found || flip_rating_[var_id] > flip_rating_[best]
				|| (flip_rating_[var_id] == flip_rating_[best]
						&& last_flip_[var_id] < last_flip_[best]))
		{
			if (best_found)
				second_best_found = true;
			best_found = true;
			second_best = best;
			best = var_id;
		}
		else if (!second_best_found
				|| flip_rating_[var_id] > flip_rating_[second_best]
				|| (flip_rating_[var_id] == flip_rating_[second_best]
						&& last_flip_[var_id] < last_flip_[second_best]))
		{
			second_best_found = true;
			second_best = var_id;
		}
	}

	if (!best_found)
		return std::make_tuple(0, 0, 0, false);

	if (!second_best_found)
		return std::make_tuple(best, best, last_flipped, true);

	return std::make_tuple(best, second_best, last_flipped, true);

}

void WalkSAT::flipAndUpdate(VarID var_id)
{
	assert(!formula_.var_fixed_[var_id]);

	bool old_val = formula_.getVar(var_id);

	formula_.flipVar(var_id);
	++flips_;

	last_flip_[var_id] = flips_;

	for (auto const &clause : formula_.var_to_clause_[var_id])
	{
		//Did the old value satisfy the clause?
		if (old_val == clause.sign)
		{
			//If yes, after the flip there is one true literal less
			--num_true_lit_[clause.clause_id];

			if (num_true_lit_[clause.clause_id] == 0)
			{
				unsatisfied_clauses_id_[clause.clause_id] = num_unsatisfied_;
				unsatisfied_clauses_.push_back(clause.clause_id);
				++num_unsatisfied_;

				//If the clause is now unsatisfied, we reset var_id's flip rating and
				//increase the flip ratings of all variables in the clause
				++flip_rating_[var_id];

				for (auto const &var : formula_.clause_to_var_[clause.clause_id])
				{
					++flip_rating_[var.var_id];
				}
			}
			else if (num_true_lit_[clause.clause_id] == 1)
			{
				//If there is now only one satisfying literal left, we decrease its flip rating
				for (auto const &var : formula_.clause_to_var_[clause.clause_id])
				{
					if (formula_.getVar(var.var_id) == var.sign)
					{
						--flip_rating_[var.var_id];
						break;
					}
				}
			}
		}
		else
		{
			//If no, after the flip there is one true literal more
			++num_true_lit_[clause.clause_id];

			if (num_true_lit_[clause.clause_id] == 1)
			{
				--num_unsatisfied_;
				unsatisfied_clauses_id_[unsatisfied_clauses_[num_unsatisfied_]] =
						unsatisfied_clauses_id_[clause.clause_id];
				std::swap(
						unsatisfied_clauses_[unsatisfied_clauses_id_[clause.clause_id]],
						unsatisfied_clauses_[num_unsatisfied_]);
				unsatisfied_clauses_.pop_back();

				//If the clause is now newly satisfied, we reset var_id's flip rating and
				//decrease the flip ratings of all variables in the clause
				--flip_rating_[var_id];

				for (auto const &var : formula_.clause_to_var_[clause.clause_id])
				{
					--flip_rating_[var.var_id];
				}
			}
			else if (num_true_lit_[clause.clause_id] == 2)
			{
				//As there is now a second satisfying literal, we reset the flip rating of the first one
				for (auto const &var : formula_.clause_to_var_[clause.clause_id])
				{
					if (formula_.getVar(var.var_id) == var.sign
							&& var.var_id != var_id)
					{
						++flip_rating_[var.var_id];
						break;
					}
				}
			}
		}
	}
}

bool WalkSAT::solve()
{
	if (formula_.contradiction_)
		return false;

	std::cout << "\n# WalkSAT\n" << std::endl;

	tries_ = 0;
	num_unsatisfied_ = formula_.numClauses();

	auto p_coin = std::bind(std::bernoulli_distribution(p), gen_);
	auto wp_coin = std::bind(std::bernoulli_distribution(wp), gen_);

	while (tries_ < MAX_TRIES && num_unsatisfied_ > 0)
	{
		generateRandomAssignment();
		evaluate();

		flips_ = 0;

		while (flips_ < MAX_FLIPS * (1 << tries_) && num_unsatisfied_ > 0)
		{

			ClauseID clause = chooseUnsatisfiedClause();

			VarID var_to_flip;

			if (wp_coin())
			{
				var_to_flip = randomVariableFromClause(clause);
			}
			else
			{
				VarID best_var;
				VarID second_best_var;
				VarID last_flipped_in_clause;
				bool success;

				std::tie(best_var, second_best_var, last_flipped_in_clause,
						success) = localSearch(clause);

				if (!success)
					break;

				if (best_var != last_flipped_in_clause || !p_coin())
				{
					var_to_flip = best_var;
				}
				else
				{
					var_to_flip = second_best_var;
				}
			}

			flipAndUpdate(var_to_flip);
		}

		++tries_;

		std::cout << "# Try " << std::setw(2) << tries_ << ": " << std::setw(9)
				<< flips_ << " flips";

		if (num_unsatisfied_ == 0)
			std::cout << ", SAT\n\n";
		else
			std::cout << ", UNSAT\n";
	}

	return formula_.evaluate();
}
