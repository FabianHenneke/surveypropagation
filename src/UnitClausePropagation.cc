#include "UnitClausePropagation.h"

UnitClausePropagation::UnitClausePropagation(SATFormula& formula) :
		formula_(formula)

{

}

bool UnitClausePropagation::reduce()
{
	bool change = true;

	while (change)
	{
		change = false;

		for (auto const &clause : formula_.clause_to_var_)
		{
			//find unit clauses
			if (clause.size() == 1)
			{
				change = true;

				//satisfy the only literal
				formula_.setVar(clause[0].var_id, clause[0].sign);

				//fix and simplify, check for contradictions
				if (!formula_.fixVar(clause[0].var_id))
					return false;
			}
		}
	}

	return true;
}
