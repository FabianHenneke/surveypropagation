/*
 * SurveyPropagation.h
 *
 *  Created on: 24.12.2014
 *      Author: fabian
 */

#ifndef SRC_SURVEYPROPAGATION_H_
#define SRC_SURVEYPROPAGATION_H_

#include <random>
#include <iostream>
#include <iomanip>
#include <cmath>

#include "SATFormula.h"
#include "WalkSAT.h"
#include "UnitClausePropagation.h"

struct SurveyPropagationBiases
{
	double pi_p;
	double pi_m;
	double pi_0;

	double W_p;
	double W_m;
	double W_0;

	double bias;
};

struct EdgeSurvey
{
	void initialize();

	double pi_u;
	double pi_s;
	double pi_0;

	double eta_f;
	double eta;

private:
	static std::default_random_engine gen_;
};

struct VarSurvey
{
	double pi_prod[2];

	std::size_t pi_prod_zeros[2];
};

class SurveyPropagation
{
public:
	SurveyPropagation() = delete;
	explicit SurveyPropagation(SATFormula& formula);

	bool simplify();

	double TOL = 1.0e-16;
	double EPS = 0.01;
	double RHO = 1.0;
	std::size_t MAX_ITERS = 100;

	bool D_BLOCK = false;
	double D_FRACTION = 0.01;
	bool COMPLEXITY = false;

private:
	void initialize();
	void computeInitialSurveys();

	bool updateSurveys();
	std::tuple<double, std::size_t> updateVarClauseSurveys(ClauseID clause_id);
	double updateClauseVarSurveys(ClauseID clause_id, double eta_prod,
			std::size_t eta_prod_zeros);

	bool decimate();
	bool decimateBlock(double fraction);

	double computeBiases();
	double computeComplexity();

	void printHeader();

	SATFormula& formula_;
	std::vector<EdgeSurvey> edge_surveys_;
	std::vector<VarSurvey> var_surveys_;

	std::vector<std::pair<SurveyPropagationBiases, EdgeID>> biases_;
	std::vector<ClauseID> order_;
};

#endif /* SRC_SURVEYPROPAGATION_H_ */
