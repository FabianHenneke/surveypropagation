/*
 * TruthValue.h
 *
 *  Created on: 23.12.2014
 *      Author: fabian
 */

#ifndef SRC_TRUTHVALUE_H_
#define SRC_TRUTHVALUE_H_

#include <iostream>

enum class TRUTH_VALUE
{
	FALSE = -1, UNDETERMINED = 0, TRUE = 1
};

TRUTH_VALUE& operator&=(TRUTH_VALUE & b1, const TRUTH_VALUE & b2);

TRUTH_VALUE& operator|=(TRUTH_VALUE & b1, const TRUTH_VALUE & b2);

TRUTH_VALUE operator&(const TRUTH_VALUE & b1, const TRUTH_VALUE & b2);

TRUTH_VALUE operator|(const TRUTH_VALUE & b1, const TRUTH_VALUE & b2);

TRUTH_VALUE operator!(const TRUTH_VALUE & b);

std::ostream& operator<<(std::ostream& stream, TRUTH_VALUE assignment);

#endif /* SRC_TRUTHVALUE_H_ */
