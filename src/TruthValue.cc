/*
 * TruthValue.cc
 *
 *  Created on: 23.12.2014
 *      Author: fabian
 */

#include "TruthValue.h"

TRUTH_VALUE& operator&=(TRUTH_VALUE & b1, const TRUTH_VALUE & b2)
{
	if (b1 == TRUTH_VALUE::TRUE && b2 == TRUTH_VALUE::TRUE)
		return b1;

	if (b1 == TRUTH_VALUE::FALSE || b2 == TRUTH_VALUE::FALSE)
		return (b1 = TRUTH_VALUE::FALSE);

	return (b1 = TRUTH_VALUE::UNDETERMINED);
}

TRUTH_VALUE& operator|=(TRUTH_VALUE & b1, const TRUTH_VALUE & b2)
{
	if (b1 == TRUTH_VALUE::TRUE || b2 == TRUTH_VALUE::TRUE)
		return (b1 = TRUTH_VALUE::TRUE);

	if (b1 == TRUTH_VALUE::FALSE && b2 == TRUTH_VALUE::FALSE)
		return b1;

	return (b1 = TRUTH_VALUE::UNDETERMINED);
}

TRUTH_VALUE operator&(const TRUTH_VALUE & b1, const TRUTH_VALUE & b2)
{
	if (b1 == TRUTH_VALUE::TRUE && b2 == TRUTH_VALUE::TRUE)
		return TRUTH_VALUE::TRUE;

	if (b1 == TRUTH_VALUE::FALSE || b2 == TRUTH_VALUE::FALSE)
		return TRUTH_VALUE::FALSE;

	return TRUTH_VALUE::UNDETERMINED;
}

TRUTH_VALUE operator|(const TRUTH_VALUE & b1, const TRUTH_VALUE & b2)
{
	if (b1 == TRUTH_VALUE::TRUE || b2 == TRUTH_VALUE::TRUE)
		return TRUTH_VALUE::TRUE;

	if (b1 == TRUTH_VALUE::FALSE && b2 == TRUTH_VALUE::FALSE)
		return TRUTH_VALUE::FALSE;

	return TRUTH_VALUE::UNDETERMINED;
}

TRUTH_VALUE operator!(const TRUTH_VALUE & b)
{
	switch (b)
	{
	case TRUTH_VALUE::FALSE:
		return TRUTH_VALUE::TRUE;
	case TRUTH_VALUE::TRUE:
		return TRUTH_VALUE::FALSE;
	default:
		return TRUTH_VALUE::UNDETERMINED;
	}
}

std::ostream& operator<<(std::ostream& stream, TRUTH_VALUE assignment)
{
	switch (assignment)
	{
	case TRUTH_VALUE::FALSE:
		stream << "false";
		break;
	case TRUTH_VALUE::TRUE:
		stream << "true";
		break;
	default:
		stream << "undetermined";
	}
	return stream;
}
