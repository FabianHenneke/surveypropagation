#include "SurveyPropagation.h"

std::default_random_engine EdgeSurvey::gen_;

void EdgeSurvey::initialize()
{
	std::uniform_real_distribution<double> rand_survey(0.0, 1.0);

	pi_u = rand_survey(gen_);
	pi_s = rand_survey(gen_);
	pi_0 = rand_survey(gen_);
	eta = rand_survey(gen_);
}

SurveyPropagation::SurveyPropagation(SATFormula& formula) :
		formula_(formula), edge_surveys_(formula_.numEdges()), var_surveys_(
				formula_.numVariables()), biases_(formula_.numVariables()), order_(
				formula_.numClauses())
{

}

void SurveyPropagation::initialize()
{
	formula_.visitClauseVarEdges([this](ClauseVarEdge& cv)
	{
		edge_surveys_[cv.edge_id].initialize();
	});
}

void SurveyPropagation::computeInitialSurveys()
{
	for (VarID var_id = 0; var_id < formula_.numVariables(); ++var_id)
	{
		var_surveys_[var_id].pi_prod[0] = 1.0;
		var_surveys_[var_id].pi_prod[1] = 1.0;

		var_surveys_[var_id].pi_prod_zeros[0] = 0;
		var_surveys_[var_id].pi_prod_zeros[1] = 0;

		formula_.visitVarClauseEdges(var_id,
				[this](VarClauseEdge& vc)
				{
					if (1 - edge_surveys_[vc.edge_id].eta > TOL)
					{
						var_surveys_[vc.var_id].pi_prod[vc.sign] *= 1 - edge_surveys_[vc.edge_id].eta;
					}
					else
					{
						++var_surveys_[vc.var_id].pi_prod_zeros[vc.sign];
					}
				});
	}
}

std::tuple<double, std::size_t> SurveyPropagation::updateVarClauseSurveys(
		ClauseID clause_id)
{
	double eta_prod = 1.0;
	std::size_t eta_prod_zeros = 0;

	formula_.visitClauseVarEdges(clause_id,
			[this, &eta_prod, &eta_prod_zeros](ClauseVarEdge& cv)
			{
				double pi_prod_u, pi_prod_s;

				double one_minus_eta = 1.0 - edge_surveys_[cv.edge_id].eta;

				pi_prod_u = var_surveys_[cv.var_id].pi_prod[1-cv.sign];

				if (var_surveys_[cv.var_id].pi_prod_zeros[cv.sign] == 0)
				{
					pi_prod_s = var_surveys_[cv.var_id].pi_prod[cv.sign] / one_minus_eta;
				}
				else if (var_surveys_[cv.var_id].pi_prod_zeros[cv.sign] == 1 && one_minus_eta < TOL)
				{
					pi_prod_s = var_surveys_[cv.var_id].pi_prod[cv.sign];
				}
				else
				{
					pi_prod_s = 0.0;
				}

				edge_surveys_[cv.edge_id].pi_u = (1.0 - RHO * pi_prod_u) * pi_prod_s;
				edge_surveys_[cv.edge_id].pi_s = (1.0 - RHO * pi_prod_s) * pi_prod_u;
				edge_surveys_[cv.edge_id].pi_0 = pi_prod_s * pi_prod_u;

				edge_surveys_[cv.edge_id].eta_f = edge_surveys_[cv.edge_id].pi_u / (edge_surveys_[cv.edge_id].pi_u + edge_surveys_[cv.edge_id].pi_s + edge_surveys_[cv.edge_id].pi_0);

				if (edge_surveys_[cv.edge_id].eta_f > TOL)
				{
					eta_prod *= edge_surveys_[cv.edge_id].eta_f;
				}
				else
				{
					++eta_prod_zeros;
				}
			});

	return std::make_tuple(eta_prod, eta_prod_zeros);
}

double SurveyPropagation::updateClauseVarSurveys(ClauseID clause_id,
		double eta_prod, std::size_t eta_prod_zeros)
{

	double epsilon = 0.0;

	formula_.visitClauseVarEdges(clause_id,
			[this, eta_prod, eta_prod_zeros, &epsilon](ClauseVarEdge& cv)
			{
				double eta_new;

				if (eta_prod_zeros == 0)
				{
					eta_new = eta_prod / edge_surveys_[cv.edge_id].eta_f;
				}
				else if (eta_prod_zeros == 1 && edge_surveys_[cv.edge_id].eta_f < TOL)
				{
					eta_new = eta_prod;
				}
				else
				{
					eta_new = 0.0;
				}

				if (1 - edge_surveys_[cv.edge_id].eta > TOL)
				{
					if (1 - eta_new > TOL)
					{
						var_surveys_[cv.var_id].pi_prod[cv.sign] *= (1 - eta_new) / (1 - edge_surveys_[cv.edge_id].eta);
					}
					else
					{
						var_surveys_[cv.var_id].pi_prod[cv.sign] /= (1 - edge_surveys_[cv.edge_id].eta);
						++var_surveys_[cv.var_id].pi_prod_zeros[cv.sign];
					}
				}
				else
				{
					if (1 - eta_new > TOL)
					{
						var_surveys_[cv.var_id].pi_prod[cv.sign] *= (1 - eta_new);
						--var_surveys_[cv.var_id].pi_prod_zeros[cv.sign];
					}
				}

				epsilon = std::max(epsilon, std::abs(eta_new - edge_surveys_[cv.edge_id].eta));

				edge_surveys_[cv.edge_id].eta = eta_new;
			});

	return epsilon;
}

bool SurveyPropagation::updateSurveys()
{
	std::default_random_engine gen;

	double epsilon = 0.0;

	for (ClauseID clause_id = formula_.numClauses(); clause_id > 0; --clause_id)
	{
		std::swap(order_[clause_id - 1],
				order_[std::uniform_int_distribution<int>(0, clause_id - 1)(gen)]);

		double eta_prod;
		std::size_t eta_prod_zeros;

		std::tie(eta_prod, eta_prod_zeros) = updateVarClauseSurveys(
				order_[clause_id - 1]);

		epsilon = std::max(epsilon,
				updateClauseVarSurveys(order_[clause_id - 1], eta_prod,
						eta_prod_zeros));
	}

	return (epsilon < EPS);
}

double SurveyPropagation::computeBiases()
{
	double total = 0;

	for (VarID var_id = 0; var_id < formula_.numVariables(); ++var_id)
	{
		double pi_prod_p = 1;
		double pi_prod_m = 1;

		formula_.visitVarClauseEdges(var_id,
				[this, &pi_prod_p, &pi_prod_m](VarClauseEdge& cv)
				{
					if (cv.sign)
					{
						pi_prod_p *= 1 - edge_surveys_[cv.edge_id].eta;
					}
					else
					{
						pi_prod_m *= 1 - edge_surveys_[cv.edge_id].eta;
					}
				});

		biases_[var_id].second = var_id;

		biases_[var_id].first.pi_p = (1 - pi_prod_p) * pi_prod_m;
		biases_[var_id].first.pi_m = (1 - pi_prod_m) * pi_prod_p;
		biases_[var_id].first.pi_0 = pi_prod_m * pi_prod_p;

		biases_[var_id].first.W_p = biases_[var_id].first.pi_p
				/ (biases_[var_id].first.pi_p + biases_[var_id].first.pi_m
						+ biases_[var_id].first.pi_0);
		biases_[var_id].first.W_m = biases_[var_id].first.pi_m
				/ (biases_[var_id].first.pi_p + biases_[var_id].first.pi_m
						+ biases_[var_id].first.pi_0);
		biases_[var_id].first.W_0 = 1 - biases_[var_id].first.W_p
				- biases_[var_id].first.W_m;

		biases_[var_id].first.bias = std::abs(
				biases_[var_id].first.W_p - biases_[var_id].first.W_m);

		total += std::max(biases_[var_id].first.W_p, biases_[var_id].first.W_m);
		//total += biases_[var_id].first.bias;
	}

	return total / formula_.numUnfixedVariables();
}

double SurveyPropagation::computeComplexity()
{
	double complexity = 0;

	for (ClauseID clause_id = 0; clause_id < formula_.numClauses(); ++clause_id)
	{
		if (formula_.clauseFixed(clause_id))
			continue;

		double pi_prod = 1.0;
		double pi_prod_u = 1.0;

		formula_.visitClauseVarEdges(clause_id,
				[this, &pi_prod, &pi_prod_u](ClauseVarEdge& cv)
				{
					pi_prod *= edge_surveys_[cv.edge_id].pi_u + edge_surveys_[cv.edge_id].pi_s + edge_surveys_[cv.edge_id].pi_0;
					pi_prod_u *= edge_surveys_[cv.edge_id].pi_u;
				});

		complexity += log(pi_prod - pi_prod_u);
	}

	for (VarID var_id = 0; var_id < formula_.numVariables(); ++var_id)
	{
		if (formula_.varFixed(var_id))
			continue;

		complexity -= (formula_.numClauses(var_id) - 1)
				* log(
						biases_[var_id].first.pi_p + biases_[var_id].first.pi_m
								+ biases_[var_id].first.pi_0);
	}

	return complexity / formula_.numUnfixedVariables();
}

bool SurveyPropagation::decimate()
{
	bool max_sign = true;
	double max_bias = -1;
	VarID max_var = 0;

	for (VarID var_id = 0; var_id < formula_.numVariables(); ++var_id)
	{
		if (formula_.varFixed(var_id))
			continue;

		if (biases_[var_id].first.bias > max_bias)
		{
			max_bias = biases_[var_id].first.bias;
			max_var = var_id;
			max_sign = (biases_[var_id].first.W_p > biases_[var_id].first.W_m);
		}
	}

	formula_.setVar(max_var, max_sign);
	formula_.fixVar(max_var);

	std::cout << std::setw(9) << max_var + 1 << " (" << (max_sign ? '+' : '-')
			<< ")";
	//std::cout << "Decimation: Set variable " << max_var + 1 << " (bias: "
	//		<< max_bias << ") to " << max_sign << std::endl << std::endl;

	return UnitClausePropagation(formula_).reduce();
}

bool SurveyPropagation::decimateBlock(double fraction)
{
	if (fraction < 0)
		return true;

	if (fraction > 1)
		fraction = 1;

	std::sort(std::begin(biases_), std::end(biases_),
			[this](std::pair<SurveyPropagationBiases, EdgeID> a, std::pair<SurveyPropagationBiases, EdgeID> b)
			{
				if (formula_.varFixed(a.second))
				return false;

				if (formula_.varFixed(b.second))
				return true;

//				return a.first.bias > b.first.bias;

				double av = 1.0 - std::min(a.first.W_p, a.first.W_m);
				double bv = 1.0 - std::min(b.first.W_p, b.first.W_m);

				return av > bv;
			});

	std::size_t to_fix = static_cast<std::size_t>(fraction
			* formula_.numUnfixedVariables());

	for (std::size_t pos = 0; pos < to_fix; ++pos)
	{
		formula_.setVar(biases_[pos].second,
				(biases_[pos].first.W_p > biases_[pos].first.W_m));
		formula_.fixVar(biases_[pos].second);

//		double norm = biases_[pos].first.W_p + biases_[pos].first.W_0
//				+ biases_[pos].first.W_m;

		/*std::cout << "H(" << biases_[pos].second + 1 << ")={"
		 << biases_[pos].first.W_p / norm << ","
		 << biases_[pos].first.W_0 / norm << ","
		 << biases_[pos].first.W_m / norm << "} ---> "
		 << ((biases_[pos].first.W_p > biases_[pos].first.W_m) ?
		 "+" : "-") << std::endl;*/
	}

	std::cout << std::setw(12) << to_fix;

	return UnitClausePropagation(formula_).reduce();
}

bool SurveyPropagation::simplify()
{
	bool converged;
	bool eta_zero = false;

	std::size_t iters;
	std::size_t steps;

	std::cout << "\n# SURVEY PROPAGATION\n# Initializing surveys..."
			<< std::endl;
	initialize();

	printHeader();

	steps = 0;

	while (formula_.numUnfixedVariables() > 0 && !eta_zero)
	{
		converged = false;
		iters = 0;
		++steps;

		computeInitialSurveys();

		for (ClauseID clause_id = 0; clause_id < formula_.numClauses();
				++clause_id)
		{
			order_[clause_id] = clause_id;
		}

		while (!converged && iters < MAX_ITERS)
		{
			converged = updateSurveys();
			++iters;
		}

		std::cout << std::setw(9) << steps << std::setw(11) << iters
				<< std::setw(11) << formula_.numUnfixedVariables();

		if (!converged)
		{
			std::cout << "\n\n# Error: SP unconverged on "
					<< formula_.numUnfixedVariables() << " variables after "
					<< iters << " iterations\n";
			break;
		}

		//std::cout << "Success: SP converged on "
		//		<< formula_.numUnfixedVariables() << " variables in " << iters
		//		<< " iterations\n";

		double avg_magnetization = computeBiases();

		double complexity = 0;
		if (COMPLEXITY)
			complexity = computeComplexity();

		std::cout << std::setw(15) << std::fixed << std::setprecision(5)
				<< avg_magnetization;

		if (avg_magnetization < EPS)
		{
			eta_zero = true;
		}
		else
		{
			bool result;

			if (D_BLOCK)
				result = decimateBlock(D_FRACTION);
			else
				result = decimate();

			if (COMPLEXITY)
			{
				if (D_BLOCK)
					std::cout << std::setw(15);
				else
					std::cout << std::setw(10);

				std::cout << std::fixed << std::setprecision(5) << complexity;
			}

			if (!result)
			{
				std::cout << "\n\n# Error: Contradiction reached\n";
				return false;
			}
		}

		std::cout << '\n';
	}

	if (eta_zero)
	{
		std::cout << "\n# Success: Trivial surveys" << std::endl;
	}
	return eta_zero;
}

void SurveyPropagation::printHeader()
{
	if (D_BLOCK)
		std::cout
				<< "\n#    step | num iter | num vars | avg. magnet. | num vars fixed";
	else
		std::cout
				<< "\n#    step | num iter | num vars | avg. magnet. | var fixed";

	if (COMPLEXITY)
		std::cout << " | complexity";

	std::cout << std::endl;
}
